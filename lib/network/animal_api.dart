import 'dart:convert';

import "package:http/http.dart" as http;
import 'package:ishelter_flutter/model/animal.dart';

class AnimalApi {
  final _baseUrl = 'https://data.coa.gov.tw/Service/OpenData';

  Future<List<Animal>> fetchAnimalList(int count) async {
    final url = Uri.parse("$_baseUrl/TransService.aspx?UnitId=QcbUEzN6E6DL&animal_status=OPEN&\$top=$count");
    final response = await http.get(url);

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      return body.map<Animal>(Animal.fromJson).toList();
    } else {
      throw Exception('Failed to get popular movie list.');
    }
  }
}