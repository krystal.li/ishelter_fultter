import 'package:ishelter_flutter/model/animal.dart';
import 'animal_api.dart';

class AnimalRepository {
  final _animalApi = AnimalApi();

  Future<List<Animal>> fetchAnimalList(int count) =>
      _animalApi.fetchAnimalList(count);
}