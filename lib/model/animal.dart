import 'package:meta/meta.dart';

class Animal {
  final int animalId;
  final String animalSubid;
  final int animalAreaPkid;
  final int animalShelterPkid;
  final String animalPlace;
  final String animalKind;
  final String animalSex;
  final String albumFile;

  const Animal({
      required this.animalId,
      required this.animalSubid,
      required this.animalAreaPkid,
      required this.animalShelterPkid,
      required this.animalPlace,
      required this.animalKind,
      required this.animalSex,
      required this.albumFile
  });

  static Animal fromJson(json) => Animal(
    animalId: json['animal_id'],
    animalSubid: json['animal_subid'],
    animalAreaPkid: json['animal_area_pkid'],
    animalShelterPkid: json['animal_shelter_pkid'],
    animalPlace: json['animal_place'],
    animalKind: json['animal_kind'],
    animalSex: json['animal_sex'],
    albumFile: json['album_file']
  );
}