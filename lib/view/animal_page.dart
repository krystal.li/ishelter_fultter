import 'dart:async';
import 'package:flutter/material.dart';
import 'package:ishelter_flutter/model/animal.dart';
import 'package:ishelter_flutter/network/animal_repository.dart';

void main() => runApp(const AnimalPage());

class AnimalPage extends StatelessWidget {
  const AnimalPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      theme: ThemeData(primarySwatch: Colors.teal),
      home: const AnimalHome(),
    );
  }
}

class AnimalHome extends StatefulWidget {
  const AnimalHome({Key? key}) : super(key: key);

  @override
  _AnimalHomeState createState() => _AnimalHomeState();
}

class _AnimalHomeState extends State<AnimalHome> {
  Future<List<Animal>>? _animals;
  final List<String> _savedAnimals = <String>[];

  @override
  void initState() {
    super.initState();

    _animals = AnimalRepository().fetchAnimalList(40);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text('iShelter with Flutter')),
        backgroundColor: Colors.white,
        body: FutureBuilder<List<Animal>>(
          future: _animals,
          builder: (context, snapshot) {
            final animals = snapshot.data;

            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return const Center(child: CircularProgressIndicator());
              default:
                if (snapshot.hasError) {
                  return const Center(child: Text('Some error occurred!'));
                } else {
                  return buildAnimals(animals!);
                }
            }
          },
        ),
      );

  Widget buildAnimals(List<Animal> animals) {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 0.75
      ),
      itemCount: animals.length,
      itemBuilder: (context, index) {
        final animal = animals[index];
        final bool alreadySaved = _savedAnimals.contains(animal.animalSubid);

        double deviceWidth = MediaQuery.of(context).size.width;
        double size = deviceWidth / 2;

        return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Stack(children: [
                  // Expanded(
                  ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(animal.albumFile,
                          width: size,
                          height: size,
                          fit: BoxFit.cover,
                          errorBuilder: (BuildContext context, Object error, StackTrace? stackTrace) {
                            return Image(
                              image: const AssetImage('assets/img_paws_tilted.png'),
                              width: size,
                              height: size,
                            );
                          })),
                  // ),
                  Align(
                      alignment: Alignment.topRight,
                      child: InkWell(
                          onTap: () {
                            setState(() {
                              toggleFavorite(animal.animalSubid);
                            });
                          },
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(0, 8, 8, 0),
                            child: Icon(
                              alreadySaved ? Icons.favorite : Icons.favorite_border,
                              color: alreadySaved ? Colors.red : Colors.white,
                            ),
                          )
                      )
                  )
                ]),
                Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                    child: Text(
                      "${animal.animalKind} ${animal.animalPlace}",
                      maxLines: 1 ,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          color: Colors.black,
                          decoration: TextDecoration.none
                      ),
                    ))
              ],
            ));
      },
    );
  }

  void toggleFavorite(String id) {
    if (_savedAnimals.contains(id)) {
      _savedAnimals.remove(id);
    } else {
      _savedAnimals.add(id);
    }
  }
}
